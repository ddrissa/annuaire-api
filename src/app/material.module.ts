import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';
import {MatTableModule} from '@angular/material/table';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatInputModule} from '@angular/material/input';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {MatAutocomplete, MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatBadgeModule} from '@angular/material/badge';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatIconModule} from '@angular/material/icon';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatDialogModule} from '@angular/material/dialog';
import {MatListModule} from '@angular/material/list';
import {MatDividerModule} from '@angular/material/divider';
import {MatMenuModule} from '@angular/material/menu';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatSelectModule} from '@angular/material/select';
import {MatChipsModule} from '@angular/material/chips';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatCardModule, MatFormFieldModule, MatButtonModule,
    MatTableModule, MatExpansionModule, MatInputModule,
    MatPaginatorModule, MatSortModule, MatAutocompleteModule,
    MatBadgeModule, MatToolbarModule, MatSidenavModule,
    MatSnackBarModule, MatIconModule, MatTooltipModule,
    MatDialogModule, MatListModule, MatDividerModule,
    MatMenuModule, MatDatepickerModule, MatSelectModule,
    MatChipsModule
  ],
  exports: [
    MatCardModule, MatFormFieldModule, MatButtonModule,
    MatTableModule, MatExpansionModule, MatInputModule,
    MatPaginatorModule, MatSortModule, MatAutocompleteModule,
    MatBadgeModule, MatToolbarModule, MatSidenavModule,
    MatSnackBarModule, MatIconModule, MatTooltipModule,
    MatDialogModule, MatListModule, MatDividerModule,
    MatMenuModule, MatDatepickerModule, MatSelectModule,
    MatChipsModule
  ]
})
export class MaterialModule { }
